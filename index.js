// alert("Hello World!");

/*
SELECTION CONTROL STRUCTURE
	- it sorts out whether the statement/s are to be executed based on the selection whether it is true or false.
	-two-way selection (True or False)
	- Multi-way selection (if else)

*/


// If ... else Statement

/*
	Syntax:
		if(condition) {
			//statement;
		} else {
			//statement;
		}

*/


	 // if statement - executes a statement if a specified condition is true

	 /*
		Syntax:
			if(condition){
				//statement;
			};

	 */

	 	let numA = -1;

	 	if(numA < 0) {
	 		 console.log("Hello");
	 	};

	 	console.log(numA < 0);


	 	let city = "New York";

	 	if(city === "New York") {
	 		console.log("Welcome to New York City!");
	 	};



	 	/*
			Else if
				- execute a statement if the previous conditions are false and the specified condition is true.
				- the "else if" clause is optional and can be added to capture additiona; condition to charge the flow of the program.

	 	*/

	 	let numB = 1;

	 	if(numA > 0) {
	 		console.log("Hello");
	 	} else if (numB > 0) {
	 		console.log("World");
	 	};


	 	city = "Tokyo";

	 	if(city === "New York") {
	 		console.log("Welcome to New York City");
	 	} else if ( city === "Tokyo") {
	 		console.log("Welcome to Toakyo!");
	 	};


/*
	else statement
		- Executes a statement if all our condition are false.
*/

	if(numA>0){
		console.log("Hello");
	} else if (numB === 0){
		console.log("World");
	} else {
		console.log("Again");
	};

	// let age = parseInt(prompt("Enter your age:"));


	// if(age <= 18){
	// 	console.log("NOT allowed to drink");
	// } else {
	// 	console.log("Matanda ka na, shot puno!");
	// };

	/*
		Mini Activty
			-create a function that will receive any value of height as an argument when you invoke it.
			-then create conditional statements:
				- if height is less than or equal to 150, print "Did not pass the min height requirement." in the console.
				- but if the height is greater than 150, print "Passed the min height requirement." in the console
	*/

	// function userHeight(height){
	// 	if (height <= 150){
	// 		console.log("Did not pass the min height requirement.");
	// 	} else {
	// 		console.log("Passed the min height requirement.");
	// 	}
	// }
	// let height = parseInt(prompt("Enter your height:"));
	// userHeight(height);


	let message = "No message";
	console.log(message);

	function determineTyphoonIntensity(windSpeed) {
		if(windSpeed<30){
			return "Not a typhoon"
		} else if (windSpeed <= 61) {
			return "Tropical depression detected"
		} else if (windSpeed >= 62 && windSpeed <= 88){
			return "Tropical storm detected"
		} else if (windSpeed>=89 && windSpeed <= 177){ 
			return "Severe tropical storm detected"
		} else {
			return"typhoon detected"
		};
	};

	message = determineTyphoonIntensity(70);
	console.log(message)

	console.log(determineTyphoonIntensity(90));
	console.log(determineTyphoonIntensity(55));
	console.log(determineTyphoonIntensity(27));
	console.log(determineTyphoonIntensity(190));

	if(message == "Tropical storm detected") {
		console.warn(message);
	}

// Truthy and Falsy

/*
	In JS a truthy value is a value that is considered true when encountered in a boolean content.

	Falsy value:
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN
*/

// Truthy Examples:

let word = "true"
if(word){
	console.log("Truthy");
};

if(true){
	console.log("Truthy");
};

if (1) {
	console.log("Truthy");
}


// Falsy Examples:

if(false) {
	console.log("Falsy");
};

if(0) {
	console.log("Falsy");
};

if(undefined) {
	console.log("Falsy");
};

if(null) {
	console.log("Falsy");
};

if(-0) {
	console.log("Falsy");
};

if(NaN) {
	console.log("Falsy");
};

// Conditional Ternary Operator - for short codes.


/*
	Ternary Operator takes 3 operands:
	1. condition
	2. expression to execute if the condition is true/truthy
	3. expression to execute if the condition is false/falsy.


	Syntax:

		(condition) ? ifTrue_expression : ifFalse_expression;
*/


//  Single statement Execution

let ternaryResult = (1 < 18) ? "Condition is True" : "Condition is False";
console.log("Result of Ternary Operator: " + ternaryResult);


// Multiple Statement Execution

let name;

function isOfLegalAge() {
	name= "John";
	return "You are of the legal age limit";
};

function isUnderAge() {
	name="Jane";
	return "You are under the age limit";
};

let yourAge = parseInt(prompt("What is your age?"));
console.log(yourAge);

let legalAge = (yourAge > 18) ? isOfLegalAge():isUnderAge();
console.log("This is the result of Ternary Operator in Function: " + legalAge + ", "+name);

